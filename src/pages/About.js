import React from 'react';
import Banner from '../components/banner/Banner';
import Collapse from '../components/collapse/Collapse';
import '../styles/about.css'
import bannerabout from '../asset/images/bannerabout.jpg'

const About = () => {
    return (
        <div className='about'>
            <Banner img={bannerabout} alt="montagne" />
            <div className="about-container">
                <Collapse collapsename="Fiabilité" contentext="Les annonces postées sur Kasa garantissent une fiabilité totale. Les photos sont conformes aux logements, et toutes les informations sont régulièrement vérifiées par nos équipes."/>
                <Collapse collapsename="Respect" contentext="La bienveillance fait partie des valeurs fondatrices de Kasa. Tout comportement discriminatoire ou de perturbation du voisinage entraînera une exclusion de notre plateforme."/>
                <Collapse collapsename="Service" contentext="Nos équipes se tiennent à votre disposition pour vous fournir une expérience parfaite. N'hésitez pas à nous contacter si vous avez la moindre question."/>
                <Collapse collapsename="Responsabilité" contentext="La sécurité est la priorité de Kasa. Aussi bien pour nos hôtes que pour les voyageurs, chaque logement correspond aux critères de sécurité établis par nos services. En laissant une note aussi bien à l'hôte qu'au locataire, cela permet à nos équipes de vérifier que les standards sont bien respectés. Nous organisons également des ateliers sur la sécurité domestique pour nos hôtes." />
            </div>
        </div>
    );
};

export default About;