import React from 'react';
import Banner from '../components/banner/Banner';
import datas from '../api/api.json';
import Card from '../components/card/Card'; 
import bannerhome from '../asset/images/bannerhome.jpg'
import '../styles/home.css';


const Home = () => {
    return (
        <div className='home'>
                <Banner img={bannerhome} alt="forêt" slogan="Chez vous, partout et ailleurs" />   
            <div className="card-container"> 
                    {
                        datas.map((data) => <Card id={data.id} title={data.title} img={data.cover} key={data.id} />)
                    }
            </div>
        </div>
    );
};

export default Home;