import React from 'react';
import { useParams } from 'react-router-dom';
import Rating from '../components/rating/Rating';
import datas from '../api/api.json';
import Slider from '../components/slider/Slider';
import Tag from '../components/tag/Tag'
import '../styles/lodging.css'
import Host from '../components/host/Host';
import Collapse from '../components/collapse/Collapse';
import ErrorPage from './404';

const Lodging = () => {
    const { id } = useParams()
    const lodging = datas.find(elem => elem.id === id)
    
    return (
        lodging ? (
        <div className='lodging'>
            <Slider  pictures={lodging.pictures}/>
            <div className="middle-container">
                <div className="left-container">
                    <h2>{lodging.title}</h2>
                    <p>{lodging.location}</p>
                    <div className="tag-container">
                    {
                        lodging.tags.map((tag, index) => <Tag tag={tag} key={index}/>)
                    }
                    </div>
                </div>
                <div className="right-container">
                <Host name={lodging.host.name} namepicture={lodging.host.picture}/>
                <Rating rating={parseInt(lodging.rating)}/>
                </div>
            </div>
            <div className="collapse-bloc">
                <div className="collapse-1">
                    <Collapse collapsename='Description' contentext={lodging.description} />
                </div>   
                <div className="collapse-2">
                    <Collapse collapsename='Equipement' contentext={lodging.equipments.map((equ, index) => <p key={index}>{equ}</p>)} />
                </div>
            </div>
        </div>
        ) : (
            <ErrorPage />
        )
    );
};

export default Lodging;