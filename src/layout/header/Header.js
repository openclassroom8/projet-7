import React from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../Logo';
import './header.css'


const Header = () => {

    return (
        <header>
            <div>
                <Logo />
            </div>
            <nav>
                <NavLink end to="/">Accueil</NavLink>       
                <NavLink to="/about">A propos</NavLink>       
            </nav>
        </header>
    );
};

export default Header;