import React from 'react';
import Logo from '../Logo';
import './footer.css'

const Footer = () => {
    return (
        <footer>
            <div>
                <Logo color="#fff" />
            </div>
            <p>© 2020 Kasa. All rights reserved</p>
        </footer>
    );
};

export default Footer;