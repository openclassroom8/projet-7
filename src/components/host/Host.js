import React from 'react';
import './host.css'

const Host = ({name, namepicture}) => {
    return (
        <div className='user'>
            <span>{name}</span>
            <img src={namepicture} alt="profil" />
        </div>
    );
};

export default Host;