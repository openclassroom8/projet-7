import React, { useState } from 'react';
import './collapse.css'
import UpIcon from '../../asset/images/icones/UpIcon';
import DownIcon from '../../asset/images/icones/DownIcon';

const Collapse = (props) => {
    const [isOpen, setIsOpen] = useState(false)

    return (
        <div className='collapse-container'>
            <button className='collapse-toggle' onClick={ () => setIsOpen(!isOpen) }>
                <span>{props.collapsename}</span>
                {isOpen ? <DownIcon /> : <UpIcon />}
                </button>
            { isOpen && <div className="collapse-content">{props.contentext}</div> }
        </div>
    );
};

export default Collapse;