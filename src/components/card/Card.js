import React from 'react';
import './card.css'

const Card = ({title, img, id}) => {
    return (
        <a href={`/lodging/${id}`} className="card">
                <img src={img} alt="Logement"/>
                <div className='overlay'></div>
                <span>{title}</span>
        </a> 
    );
};

export default Card;