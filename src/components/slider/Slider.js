import React, { useEffect, useState } from 'react';
import LeftIcon from '../../asset/images/icones/LeftIcon';
import RightIcon from '../../asset/images/icones/RightIcon';
import "./slider.css"



const Slider = ({pictures}) => {
    const [step, setStep] = useState(0)
    const lastIndex = pictures.length -1
    const nextImg = () => {
        setStep(step + 1)
    }

    const prevImg = () => {
        setStep(step - 1)
    }

    useEffect(() => { 
        if(step > lastIndex) {
            setStep(0)
        }
        if(step < 0) {
            setStep(lastIndex)
        }
    }, [step])



    return (
        <div className="slider">
            <div className="previous" onClick={() => prevImg()}>
                {pictures.length > 1 && <LeftIcon />}
            </div>
            <img src={pictures[step]} alt='appartement' className='img-slider'></img>
            <div className="next" onClick={() => nextImg()}>
                {pictures.length > 1 && <RightIcon />}
            </div>
            <div className="numberImg">
                <span>{step + 1}/{lastIndex + 1}</span>
            </div>
        </div>
    );
};

export default Slider;