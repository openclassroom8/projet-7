import React from 'react';
import './tag.css'

const tag = ({tag}) => {
    return (
        <div className='tag'>
            <span>{tag}</span>
        </div>
    );
};

export default tag;