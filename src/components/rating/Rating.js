import React from 'react';
import './rating.css'
import { IoMdStar } from "react-icons/io";


const Rating = ({rating}) => {
    return (
        <div className="rating-container">
            {[...Array(5)].map((star, i) => {
                const ratingValue = i + 1

                return (    
                        <IoMdStar 
                            className='star' 
                            size={30} 
                            color={ratingValue <= rating ? "#FF6060" : "#E3E3E3"}
                            key={i}
                            /> 
                )
                })}
        </div>
    );
};

export default Rating;