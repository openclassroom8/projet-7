import React from 'react';
import './banner.css'

const Banner = ({img, alt, slogan}) => {
    return (
        <div className="banner">
            <img src={img} alt={alt} />
            <p>{slogan}</p>
        </div>
    );
};

export default Banner;