import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Home from './pages/Home';
import About from './pages/About';
import ErrorPage from './pages/404';
import Lodging from './pages/Lodging';
import Header from './layout/header/Header';
import Footer from './layout/footer/Footer';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
        <BrowserRouter>
          <Header />
          <main>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route  path="/about" element={<About />} />
            <Route  path="/lodging/:id" element={<Lodging />} />
            <Route  path="/*" element={<ErrorPage />} /> 
          </Routes> 
          </main>
        </BrowserRouter>
      <Footer />
  </React.StrictMode>
);

reportWebVitals();
