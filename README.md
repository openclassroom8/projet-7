# Kasa 

## Présentation
Ce projet présente un site web de mise en location d'appartements entre particuliers.

## Mise en route
### Installation des dépendances 
Pour installer les dépendances, il faut éxecuter la commande 
``` npm install ```


### Lancement du projet
Pour lancer le projet il faut éxecuter la commande 
```npm start ```